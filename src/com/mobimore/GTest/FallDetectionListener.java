package com.mobimore.GTest;

import com.mobimore.graphicsObjects.kinect.SkeletonDrawable;
import com.mobimore.utils.LibSVM;

//Listener for fall detection algo events
public interface FallDetectionListener {
    void onFallDetected(SkeletonDrawable source, int frameNumber);
    void onFallUndetected(SkeletonDrawable source, int frameNumber);
    void onNewFrameProcessed(SkeletonDrawable source, boolean expertVal, LibSVM.SVMResult svmResult, boolean resultVal, int frameNumber);
}
