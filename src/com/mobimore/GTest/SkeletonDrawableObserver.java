package com.mobimore.GTest;

import com.mobimore.graphicsObjects.kinect.SkeletonDrawable;

//Listen to scene start/stop events
public interface SkeletonDrawableObserver {
    void onSceneEnded(SkeletonDrawable sender);
    void onSceneStarted(SkeletonDrawable sender);
}
