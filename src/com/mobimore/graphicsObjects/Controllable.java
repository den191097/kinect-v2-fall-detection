package com.mobimore.graphicsObjects;

/**
 * Functions to control rendering step by step
 */
public interface Controllable {
    void pause();
    void resume();
    void restart();
    void stepBack();
    void stepFW();
    int getCurrentFrameNumber();
    boolean isPaused();
}
