package com.mobimore.graphicsObjects.kinect;

import com.mobimore.GTest.FallDetectionListener;
import com.mobimore.GTest.SkeletonDrawableObserver;
import com.mobimore.camera.Camera;
import com.mobimore.graphicsObjects.Controllable;
import com.mobimore.graphicsObjects.Drawable;
import com.mobimore.graphicsObjects.waveFront.Vect3D;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * Draw Kinect V2 depth map and skeleton joints to screen
 */
public class KinectDrawable extends Drawable implements Controllable {
    public static final String FALL_MARKS_CSV = "fallMarks.csv";
    private DepthDrawable depthDrawable;
    private SkeletonDrawable skeletonDrawable;
    private SkeletonDrawable skeletonDrawableSkSpace;
    private int fallStartedFrame = -1;
    private int fallEndedFrame = -1;
    private double maxHeight = -1;

    public String getFolderWithFiles() {
        return folderWithFiles;
    }

    private String folderWithFiles;

    public void setFallDetectionListener(FallDetectionListener fallDetectionListener) {
        this.skeletonDrawableSkSpace.setFallDetectionListener(fallDetectionListener);
    }

    public void setSkeletonDrawableObserver(SkeletonDrawableObserver observer){
        this.skeletonDrawableSkSpace.setObserver(observer);
    }

    public static KinectDrawable fromFolder(File folder, String name){
        String folderWithFiles = folder.getAbsolutePath() + File.separatorChar;
        DepthDrawable depthDrawable = DepthDrawable.fromFolder(new File(folderWithFiles + "Depth"), name + "_depth");
        SkeletonDrawable skeletonDrawable = SkeletonDrawable.fromCSVFile(new File(folderWithFiles + "Body" + File.separatorChar + "Fileskeleton.csv"), name + "skeleton", false);
        SkeletonDrawable skeletonDrawableSkSpace = SkeletonDrawable.fromCSVFile(new File(folderWithFiles + "Body" + File.separatorChar + "FileskeletonSkSpace.csv"), name + "skeletonSkSpace", true);
        KinectDrawable kinectDrawable = new KinectDrawable(name, folderWithFiles);
        kinectDrawable.depthDrawable = depthDrawable;
        kinectDrawable.skeletonDrawable = skeletonDrawable;
        kinectDrawable.skeletonDrawableSkSpace = skeletonDrawableSkSpace;
        kinectDrawable.skeletonDrawable.setFallStartFrame(kinectDrawable.fallStartedFrame);
        kinectDrawable.skeletonDrawable.setFallEndFrame(kinectDrawable.fallEndedFrame);
        kinectDrawable.skeletonDrawableSkSpace.setFallStartFrame(kinectDrawable.fallStartedFrame);
        kinectDrawable.skeletonDrawableSkSpace.setFallEndFrame(kinectDrawable.fallEndedFrame);
        kinectDrawable.maxHeight = skeletonDrawableSkSpace.getMaxHeight();

        return kinectDrawable;
    }

    //read expert mark with fall start and fall end
    private void readFallMarks(File file) throws IOException {
        List<String> lines = Files.readAllLines(file.toPath());
        String[] line = lines.get(1).split(", ");
        fallStartedFrame = Integer.parseInt(line[0]);
        fallEndedFrame = Integer.parseInt(line[1]);
    }

    private KinectDrawable(String name, String folderWithFiles) {
        setName(name);
        this.folderWithFiles = folderWithFiles;
        File fallMarksFile = new File(folderWithFiles + FALL_MARKS_CSV);
        if(fallMarksFile.exists()){
            try {
                readFallMarks(fallMarksFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void paint(Graphics graphics, Camera camera, int width, int height) {
        if (depthDrawable != null) {
            depthDrawable.paint(graphics, camera, width, height);
        }

        if (skeletonDrawable != null) {
            skeletonDrawable.paint(graphics, camera, width, height);
        }

        if (skeletonDrawableSkSpace != null) {
            skeletonDrawableSkSpace.paint(graphics, camera, width, height);

        }
    }

    @Override
    public void setColor(Color color) {
        skeletonDrawable.setColor(color);
    }

    @Override
    public Color getColor() {
        return skeletonDrawable.getColor();
    }

    @Override
    public void pause() {
        if(depthDrawable!=null){
            depthDrawable.pause();
        }

        if (skeletonDrawable != null) {
            skeletonDrawable.pause();
        }

        if (skeletonDrawableSkSpace != null) {
            skeletonDrawableSkSpace.pause();
        }
    }

    @Override
    public void resume() {
        if(depthDrawable!=null){
            depthDrawable.resume();
        }

        if (skeletonDrawable != null) {
            skeletonDrawable.resume();
        }

        if (skeletonDrawableSkSpace != null) {
            skeletonDrawableSkSpace.resume();
        }
    }

    @Override
    public void restart() {
        if(depthDrawable!=null){
            depthDrawable.restart();
        }

        if (skeletonDrawable != null) {
            skeletonDrawable.restart();
        }

        if (skeletonDrawableSkSpace != null) {
            skeletonDrawableSkSpace.restart();
        }
    }

    @Override
    public void stepBack() {
        if(depthDrawable!=null){
            depthDrawable.stepBack();
        }

        if (skeletonDrawable != null) {
            skeletonDrawable.stepBack();
        }

        if (skeletonDrawableSkSpace != null) {
            skeletonDrawableSkSpace.stepBack();
        }
    }

    @Override
    public void stepFW() {
        if(depthDrawable!=null){
            depthDrawable.stepFW();
        }

        if (skeletonDrawable != null) {
            skeletonDrawable.stepFW();
        }

        if (skeletonDrawableSkSpace != null) {
            skeletonDrawableSkSpace.stepFW();
        }
    }

    @Override
    public int getCurrentFrameNumber() {
        if (skeletonDrawable != null) {
            return skeletonDrawable.getCurrentFrameNumber();
        }
        return -1;
    }

    @Override
    public boolean isPaused() {
        return depthDrawable.isPaused() && skeletonDrawable.isPaused();
    }

    public int getFallStartedFrame() {
        return fallStartedFrame;
    }

    public void setFallStartedFrame(int fallStartedFrame) {
        this.fallStartedFrame = fallStartedFrame;
        this.skeletonDrawable.setFallStartFrame(fallStartedFrame);
    }

    public int getFallEndedFrame() {
        return fallEndedFrame;
    }

    public void setFallEndedFrame(int fallEndedFrame) {
        this.fallEndedFrame = fallEndedFrame;
        this.skeletonDrawable.setFallEndFrame(fallEndedFrame);
    }

    public double getMaxHeight() {
        return maxHeight;
    }

    //save expert fall start and end mark to file
    public void saveDataToFolder() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("fallStartedFrame, ").append("fallEndedFrame, ").append("height\n");
        stringBuilder.append(fallStartedFrame).append(", ").append(fallEndedFrame).append(", ").append(String.format("%.3f", maxHeight));
        //System.out.println(stringBuilder);
        File file = new File(folderWithFiles + FALL_MARKS_CSV);
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(stringBuilder.toString());
            fileWriter.flush();
        }
    }

    //get/set bounds for Pages algo

    public double getLowerBound() {
        if (skeletonDrawableSkSpace != null) {
            return skeletonDrawableSkSpace.getLowerBound();
        } else {
            return 1;
        }
    }

    public void setLowerBound(double lowerBound) {
        if(skeletonDrawableSkSpace!=null){
            skeletonDrawableSkSpace.setLowerBound(lowerBound);
        }
    }

    public double getUpperBound() {
        if (skeletonDrawableSkSpace != null) {
            return skeletonDrawableSkSpace.getUpperBound();
        }else{
            return -1;
        }
    }

    public void setUpperBound(double upperBound) {
        if (skeletonDrawableSkSpace != null) {
            skeletonDrawableSkSpace.setUpperBound(upperBound);
        }
    }

    //

    public int getFramesCount(){
        if (skeletonDrawableSkSpace != null) {
            return skeletonDrawableSkSpace.getFramesCount();
        }else{
            return 0;
        }
    }
}
